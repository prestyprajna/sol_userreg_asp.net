﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication1.Models.ORD.Customer
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="UserDb")]
	public partial class CustomerDCDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    #endregion
		
		public CustomerDCDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["UserDbConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public CustomerDCDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public CustomerDCDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public CustomerDCDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public CustomerDCDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.uspSetCustomer")]
		public int uspSetCustomer([global::System.Data.Linq.Mapping.ParameterAttribute(Name="Command", DbType="VarChar(50)")] string command, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="PersonId", DbType="Decimal(18,0)")] System.Nullable<decimal> personId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="FirstName", DbType="VarChar(50)")] string firstName, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="LastName", DbType="VarChar(50)")] string lastName, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Username", DbType="VarChar(50)")] string username, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Password", DbType="VarChar(50)")] string password, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Usetype", DbType="VarChar(50)")] string usetype, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="MobileNo", DbType="VarChar(10)")] string mobileNo, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="EmailId", DbType="VarChar(100)")] string emailId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Status", DbType="Int")] ref System.Nullable<int> status, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Message", DbType="VarChar(MAX)")] ref string message)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), command, personId, firstName, lastName, username, password, usetype, mobileNo, emailId, status, message);
			status = ((System.Nullable<int>)(result.GetParameterValue(9)));
			message = ((string)(result.GetParameterValue(10)));
			return ((int)(result.ReturnValue));
		}
	}
}
#pragma warning restore 1591
