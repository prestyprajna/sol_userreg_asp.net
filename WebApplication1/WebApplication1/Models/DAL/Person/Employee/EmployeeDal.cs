﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models.Entity.Person.Employee;
using WebApplication1.Models.ORD.Employee;

namespace WebApplication1.Models.DAL.Person.Employee
{
    public class EmployeeDal
    {
        #region  declaration
        private EmployeeDCDataContext _dc = null;
        #endregion

        #region  constructor
        public EmployeeDal()
        {
            _dc = new EmployeeDCDataContext();
        }

        #endregion

        #region  public method
        public dynamic AddEmployeeData(EmployeeEntity employeeEntityObj)  //add employee data to database
        {
            int? status = null;
            string message = null;

            var setQuery =
                _dc
                ?.uspSetEmployee(
                    "INSERT",
                    employeeEntityObj?.personEntityObj?.PersonId,
                    employeeEntityObj?.personEntityObj?.FirstName,
                    employeeEntityObj?.personEntityObj?.LastName,
                    employeeEntityObj?.personEntityObj?.loginEntityObj?.Username,
                    employeeEntityObj?.personEntityObj?.loginEntityObj?.Password,
                    employeeEntityObj?.personEntityObj?.loginEntityObj?.Usetype,
                    employeeEntityObj?.personEntityObj?.communicationEntityObj?.MobileNo,
                    employeeEntityObj?.personEntityObj?.communicationEntityObj?.EmailId,
                    ref status,
                    ref message
                    );

            return (status == 1) ? true : false;

        }

        #endregion
    }
}
