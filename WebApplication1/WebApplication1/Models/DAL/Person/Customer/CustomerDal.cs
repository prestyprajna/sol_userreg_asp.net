﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models.Entity.Person.Customer;
using WebApplication1.Models.ORD.Customer;

namespace WebApplication1.Models.DAL.Person.Customer
{
    public class CustomerDal
    {
        #region  declaration
        private CustomerDCDataContext _dc = null;
        #endregion

        #region  constructor
        public CustomerDal()
        {
            _dc = new CustomerDCDataContext();
        }

        #endregion

        #region  public method
        public dynamic AddCustomerData(CustomerEntity customerEntityObj)  //add customer data to database
        {
            int? status = null;
            string message = null;

            var setQuery=
                _dc
                ?.uspSetCustomer(
                    "INSERT",
                    customerEntityObj?.personEntityObj?.PersonId,
                    customerEntityObj?.personEntityObj?.FirstName,
                    customerEntityObj?.personEntityObj?.LastName,
                    customerEntityObj?.personEntityObj?.loginEntityObj?.Username,
                    customerEntityObj?.personEntityObj?.loginEntityObj?.Password,
                    customerEntityObj?.personEntityObj?.loginEntityObj?.Usetype,
                    customerEntityObj?.personEntityObj?.communicationEntityObj?.MobileNo,
                    customerEntityObj?.personEntityObj?.communicationEntityObj?.EmailId,
                    ref status,
                    ref message
                    );

            return (status == 1) ? true : false;

        }

        #endregion
    }
}