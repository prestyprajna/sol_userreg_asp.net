﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.Entity.Person
{
    public class PersonEntity
    {
        public decimal? PersonId { get; set; }

        
        public string FirstName { get; set; }

        
        public string LastName { get; set; }

       
        public LoginEntity loginEntityObj { get; set; }

        
        public CommunicationEntity communicationEntityObj { get; set; }
    }
}