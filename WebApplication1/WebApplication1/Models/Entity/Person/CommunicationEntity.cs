﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.Entity.Person
{
    public class CommunicationEntity
    {
        public decimal? PersonId { get; set; }

       
        public string MobileNo { get; set; }

        
        public string EmailId { get; set; }
    }
}