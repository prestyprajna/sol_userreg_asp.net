﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Models.DAL.Person.Customer;
using WebApplication1.Models.Entity.Person;
using WebApplication1.Models.Entity.Person.Customer;

namespace WebApplication1
{
    public partial class CustomerPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            bool result= this.RegisterCustomerData();

            this.CheckInsertSuccesssfull(result);
        }


        #region   private methods

        private bool RegisterCustomerData()  //used to bind data from textbox to entity
        {
            CustomerEntity customerEntityObj = new CustomerEntity()
            {
                personEntityObj = new PersonEntity()
                {
                    FirstName = txtFirstName.Text,
                    LastName = txtLastName.Text,
                    loginEntityObj = new LoginEntity()
                    {
                        Username = txtUsername.Text,
                        Password = txtPassword.Text,
                        Usetype = txtUsetype.Text
                    },
                    communicationEntityObj = new CommunicationEntity()
                    {
                        EmailId = txtEmailId.Text,
                        MobileNo = txtMobileNo.Text
                    }
                }
            };
            var flag=new CustomerDal().AddCustomerData(customerEntityObj);

            return flag;
        }

        private void CheckInsertSuccesssfull(bool val)
        {
            if(val==true)
            {
                Response.Redirect("~/Logout page.aspx");
            }
            else
            {
                lblMessage.Text = "*please enter the data again";
            }
        }

        #endregion
    }
}