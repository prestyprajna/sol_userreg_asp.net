﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logout page.aspx.cs" Inherits="WebApplication1.Logout_page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblMessage" runat="server" Text="Data Inserted Successfully"></asp:Label>
                </td>
            </tr>

             <tr>
                <td>
                    <asp:Button ID="btnLogout" runat="server" Text="Logout" BackColor="YellowGreen" OnClick="btnLogout_Click" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
