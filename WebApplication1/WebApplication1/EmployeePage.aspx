﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeePage.aspx.cs" Inherits="WebApplication1.EmployeePage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <table>

            <tr>
                <td>
                     <span>PERSONAL DETAILS</span>
                </td>               
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblFirstName" runat="server" Text="FirstName:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblLastName" runat="server" Text="LastName:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                </td>
            </tr>

             <tr>
                <td>
                    <br />
                </td>
            </tr>
                       

            <tr>
                <td>
                     <span>LOGIN DETAILS</span>
                </td>               
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblUsername" runat="server" Text="Username:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblPassword" runat="server" Text="Password:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblUseType" runat="server" Text="UseType:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtUsetype" runat="server"></asp:TextBox>
                </td>
            </tr>

             <tr>
                <td>
                    <br />
                </td>
            </tr>

            <tr>
                <td>
                     <span>COMMUNICATION DETAILS</span>
                </td>               
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblMobileNo" runat="server" Text="Mobile No:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtMobileNo" runat="server" MaxLength="10"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblEmailId" runat="server" Text="EmailId:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEmailId" runat="server" TextMode="Email"></asp:TextBox>
                </td>
            </tr>

             <tr>
                <td>
                    <br />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"/>
                </td>                
            </tr>

            <tr>
                <td>
                    <br />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblMessage" runat="server" Font-Size="Smaller" ForeColor="DarkRed"></asp:Label>
                </td>
            </tr>



        </table>
    
    </div>
    </form>
</body>
</html>
